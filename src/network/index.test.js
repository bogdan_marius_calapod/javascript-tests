/**
 * Tests for simple network requests mock
 */

const moxios = require('moxios')
const { ajaxCall } = require('./')

describe('Network Requests', () => {
  beforeEach(() => moxios.install())
  afterEach(() => moxios.uninstall())

  it('makes a request correctly', async () => {
    const data = {
      status: 200,
      response: {
        value: 5
      }
    }

    moxios.wait(async () => moxios.requests.mostRecent().respondWith(data))

    let response = await ajaxCall()
    expect(response).toEqual({ value: 5 })
  })
})
