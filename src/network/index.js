/**
 * Simple network requests mock
 */

const axios = require('axios')

/**
 * Makes a GET request on the URL and returns the response
 *
 * @param {String} url
 */
async function ajaxCall (url = '') {
  let res

  try {
    res = await axios.get(url)
  } catch (e) {
    throw new Error('Error while making GET request')
  }

  return res.data
}

module.exports = {
  ajaxCall
}
