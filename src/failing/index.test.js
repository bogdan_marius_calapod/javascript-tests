/**
 * Tests that always fail
 */

describe('Always failing tests', () => {
  it('expects false to be true', () => expect(false).toBe(true))
})
