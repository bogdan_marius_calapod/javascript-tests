/**
 * Tests for simple arithmetic functions
 */

const { add, multiply, percentageOf } = require('./')

describe('Arithmetic functions', () => {
  describe('Add function', () => {
    it('adds two numbers correctly', () => {
      expect(add(5, 3)).toEqual(8)
    })
  })

  describe('Multiply function', () => {
    it('multiplies two numbers correctly', () => {
      expect(multiply(10, 3)).toEqual(30)
    })
  })

  describe('PercentageOf function', () => {
    it('calculates percentages correctly', () => {
      expect(percentageOf(5, 10)).toEqual(50)
    })
  })
})
