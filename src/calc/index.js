/**
 * Simple arithmetic functions
 */

/**
 * Adds two numbers together
 *
 * @param {Number} x
 * @param {Number} y
 */
function add (x, y) {
  return x + y
}

/**
 * Multiplies two numbers together
 *
 * @param {Number} x
 * @param {Number} y
 */
function multiply (x, y) {
  return x * y
}

/**
 * Calculates one number as a percentage of another
 *
 * @param {Number} x
 * @param {Number} y
 */
function percentageOf (x, y) {
  return x / 100 * y * 100
}

module.exports = {
  add,
  multiply,
  percentageOf
}
