# Javascript Tests

Simple repo with dummy tests

# Installation

## Prerequisites

In order to install and run, you will need the following:

* [NodeJS](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) > `v9.9.0`
* [Yarn](https://yarnpkg.com/en/docs/install) > `v1.5.1`

**Note:** You can also install Yarn by running `npm install --global yarn` as root.

## Installing packages

To install required packages, `cd` into the repo root folder and run the following command:

```
yarn
```

This will install all required dependencies.

# Structure

All the source files are located in the `src/` subfolder. The files are organized by the
category of functions they are running.

* `calc/` - Simple arithmetic functions
* `network/` - Some AJAX calls with [axios](https://github.com/axios/axios)
* `failing/` - A couple of basic tests that always fail

# Testing

## Framework used

The tests are written using [`Jest`](https://facebook.github.io/jest/), a JavaScript testing framework.

## Running passing tests

In order to run the provided tests you can run the following command:

```
yarn test
```

This will run `Jest` and generate a test and coverage report.

## Running failing tests

Included in this repo is also a suite of failing tests, to check CI integration.

In order to run these tests, execute the following command:

```
yarn test-failing
```
